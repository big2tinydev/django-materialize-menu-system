# Generated by Django 2.2 on 2019-11-20 16:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('menu_system', '0007_sociallink_icon_name'),
    ]

    operations = [
        migrations.DeleteModel(
            name='WebsiteInfo',
        ),
    ]
