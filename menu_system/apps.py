from django.apps import AppConfig


class MenuSystemConfig(AppConfig):
    name = 'menu_system'
