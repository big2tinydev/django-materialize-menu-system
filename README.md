Django & Materialize Menu System
=====

"Django Materialize Menu System" is a simple Django app that adds a
 customizable menu system toolbar to your Django site.  This requires a
  django projects to be installed into.
  
  in your terminal:
```bash
django-admin startproject <project name>
```

Within the admin panel, you can add all the links you need within the (4
) four database tables. If you add auth to your site, you can use the
 accounts required fields to controll the visibility of the menu system links.
 
You can also change the toolbar colors by adding settings.py variables.


1.  Main Menu links that are located in the Main Menu side drawer.
2. User links that is located in the User link side drawer.
3. App links that are located in the App link side drawer.
4. Social links that are located in the Social link side drawer.

Detailed documentation will be coming soon and will be located in the Docs
 directory.

Quick start
-----------

- Add "django-materialize-menu-system" to your INSTALLED_APPS setting like this:

settings.py
```python
INSTALLED_APPS = [
    ...
    'django.contrib.humanize',
    'menu_system',
    ...    ]            

```

- Make sure the TEMPLATE DIRS path is matching below and the
 context_processors are added as shown below.

settings.py
```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [ os.path.join( BASE_DIR, 'templates' ) ]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                ...
                'menu_system.context_processors.WebsiteInfo', <<---
                'menu_system.context_processors.user_context', <<---
                'menu_system.context_processors.social_context', <<---
                'menu_system.context_processors.menu_context', <<---
                'menu_system.context_processors.app_context', <<---
                ...
                ],
            },
        },
    ]

```

- Add the below variables to your projects settings.py

settings.py
```python
# Website Info
WEBSITE_NAME = 'Enter your site name here'
WEBSITE_SHORT_NAME = 'Enter the short name for your site'
WEBSITE_TAGLINE = 'Enter a company tagline if you have one'
MENU_COLOR = 'Enter Materialize Base colors here'
COMPANY_WEBSITE = 'Enter your website address here'
```

See Materialize docs for colors: https://materializecss.com/color.html

- When adding the Menu system to your Django templates, make sure to add the
 "Load Static" tag, the "Block tag" and the "include tag" as shown below.

Your base html file:
```djangotemplate
{% extends 'menu_system/base.html' %}
{% load static %}

{% block menu %}
	{% include "menu_system/navbar_main.html" %}
{% endblock menu %}

{% block content %}

{% endblock content %}
```

- Run ```python manage.py migrate``` to create the "Menu System" models.

- Customize the "Website Info" in your projects settings.py

- Start the development server and visit http://127.0.0.1:8000/admin/menu_system/
   to start adding the links you need in your project. (you'll need the Admin
    app enabled).
